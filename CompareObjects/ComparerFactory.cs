﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CompareObjects
{
    public class ComparerFactory
    {
        private readonly static string[] dataTypes = { "int32", "int32?", "double", "double?", "string", "bool", "bool?", "datetime","char" };
        private readonly static string[] dataTypesContainers = { "list`1", "[]" };


        public List<Models.Comparer> CompareObjectsPropertes(object t1,object t2,PropertyInfo[] properties1,PropertyInfo[] properties2)
        {
            List<Models.Comparer> result = new List<Models.Comparer>();


            if ((properties1 == null && properties2 == null) ||
                (properties1.Length==0 && properties2.Length==0) 
                )
            {
                if(t1.Equals(t2))
                {
                    result.Add(new Models.Comparer()
                    {
                        Prop1 = null,
                        Prop2 = null,
                        AreSame = true,
                        DifferentAttribute = $"object with type:'{t1.GetType().Name}' value in object 1 is same like in object 2"
                    });
                }
                else
                {
                    result.Add(new Models.Comparer()
                    {
                        Prop1 = null,
                        Prop2 = null,
                        AreSame = false,
                        DifferentAttribute = $"object with type:'{t1.GetType().Name}' value in object 1 is not same like in object 2"
                    });
                }
                return result;
            }
            if ((properties1 == null && properties2 != null) ||
                (properties1 != null && properties2 == null) ||
                (properties1.Length==0 && properties2.Length!=0) ||
                (properties1.Length!=0 && properties2.Length==0)  )
            {
                result.Add(new Models.Comparer()
                {
                    Prop1 = null,
                    Prop2 = null,
                    AreSame = false,
                    DifferentAttribute = $"object 1 has different counts of properties than in object2"
                });
                return result;
            }
            foreach(var prop1 in properties1)
            {
                //na začátku potřebuju nastavit podmínky kdy výpočet má skončit, resp. kdy rekurse končí
                var prop2 = GetProperty(properties2, prop1.Name);
                if (prop2==null)
                {
                    result.Add(new Models.Comparer()
                    {
                        Prop1 = prop1,
                        Prop2 = prop2,
                        AreSame = false,
                        DifferentAttribute = $" property name p1 '{prop1.Name}' is not in p2 "
                    });
                    continue;
                }
                if(prop1.GetType()!=prop2.GetType())
                {
                    result.Add(new Models.Comparer()
                    {
                        Prop1 = prop1,
                        Prop2 = prop2,
                        AreSame = false,
                        DifferentAttribute = $" property type p1 '{prop1.Name}' is not same in p2 "
                    });
                    continue;
                }

                //pokud je prop1 datový typ definovaný výše, tak porovnávám hodnoty
                if (dataTypes.Contains(prop1.PropertyType.Name.ToLower()))
                {
                    //prop1.GetValue(t1.GetType().GetProperty("c").GetValue(t1, null),null)
                    if (
                        (prop1.GetValue(t1, null)==null && prop2.GetValue(t2,null)!=null )
                        || 
                        (prop1.GetValue(t1, null) != null && prop2.GetValue(t2, null) ==null))
                    {
                        result.Add(new Models.Comparer()
                        {
                            Prop1 = prop1,
                            Prop2 = prop2,
                            AreSame = false,
                            DifferentAttribute = $" property name p1 '{prop1.Name}' has not same value like in p2"
                        });
                        continue;
                    }

                    if(prop1.GetValue(t1,null)==null && prop2.GetValue(t2,null)==null)
                    {
                        result.Add(new Models.Comparer()
                        {
                            Prop1 = prop1,
                            Prop2 = prop2,
                            AreSame = true,
                            DifferentAttribute = $" property '{prop1.Name}' are same in p1 and p2"
                        });
                        continue;
                    }

                    if ((!prop1.GetValue(t1, null).Equals(prop2.GetValue(t2, null))))
                    {
                        result.Add(new Models.Comparer()
                        {
                            Prop1 = prop1,
                            Prop2 = prop2,
                            AreSame = false,
                            DifferentAttribute = $" property name p1 '{prop1.Name}' has not same value like in p2"
                        });
                        continue;
                    }
                    result.Add(new Models.Comparer()
                    {
                        Prop1 = prop1,
                        Prop2 = prop2,
                        AreSame = true,
                        DifferentAttribute = $" property '{prop1.Name}' are same in p1 and p2"
                    });
                    continue;
                }
                var returnedObj1 = prop1.GetValue(t1, null);
                var returnedObj2 = prop2.GetValue(t2, null);
                if (returnedObj1 is IEnumerable) //pokud to je list/array atd..
                {
                    var list1 = returnedObj1 as IList;
                    var list2 = returnedObj2 as IList;
                    
                    if(list1.Count!=list2.Count)
                    {
                        result.Add(new Models.Comparer()
                        {
                            Prop1 = prop1,
                            Prop2 = prop2,
                            AreSame = true,
                            DifferentAttribute = $" property '{prop1.Name}' has different lenght as prop2"
                        });
                        continue;
                    }
                   for(int i=0;i<list1.Count;i++)
                    {
                        if(dataTypes.Contains(list1[i].GetType().Name.ToLower().ToString()))
                        {
                            result.AddRange(CompareObjectsPropertes(list1[i], list2[i],
                             null, null));
                        }
                    }
                }
                else //jinak volám rekurzi a procházím její properties
                    result.AddRange(CompareObjectsPropertes(t1.GetType().GetProperty(prop1.Name).GetValue(t1, null), t2.GetType().GetProperty(prop2.Name).GetValue(t2, null),
                        t1.GetType().GetProperty(prop1.Name).GetValue(t1, null).GetType().GetProperties(),
                        t2.GetType().GetProperty(prop2.Name).GetValue(t2, null).GetType().GetProperties()));

          }
            return result;
            
        }

        public PropertyInfo GetProperty(PropertyInfo[] properties,string propertyName)
        {
            foreach(var prop in properties)
            {
                if (prop.Name.ToString() == propertyName)
                    return prop;
            }
            return null;
        }

         
    }
}
