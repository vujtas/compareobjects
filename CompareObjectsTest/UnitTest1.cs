﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CompareObjectsTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            CompareObjects.ComparerFactory c = new CompareObjects.ComparerFactory();
            A a = new A()
            {
                b="ahoj",
                c=new C()
                {
                    x=1
                },
                x=0
            };
            B b = new B()
            {
                b="test",
                c=new C()
                {
                    x=1
                },
                t=0,
                x="ahoj"
            };
            c.CompareObjectsPropertes(a,b,a.GetType().GetProperties(), b.GetType().GetProperties());
        }

        [TestMethod]
        public void TestMethod2Test()
        {
            string obj1text = System.IO.File.ReadAllText("InputFiles/deflist.json");
            var obj1= JsonConvert.DeserializeObject<DefList>(obj1text);

            string obj2text = System.IO.File.ReadAllText("InputFiles/plc.json");
            var obj2 = JsonConvert.DeserializeObject<DefList>(obj2text);

            CompareObjects.ComparerFactory c = new CompareObjects.ComparerFactory();
            c.CompareObjectsPropertes(obj1, obj2, obj1.GetType().GetProperties(), obj2.GetType().GetProperties());

        }

        public class DefList
        {
            public string TypeDataType { get; set; }
            public string Name { get; set; }
            public string Mask { get; set; }
            public List<DefListParams> Params { get; set; }
        }

        public class DefListParams
        {
            public string Name { get; set; }
            public string DataType { get; set; }
            public string DefaultValue { get; set; }
            public string Description_1 { get; set; }
            public string Description_2 { get; set; }
            public int PlcByte { get; set; }
            public int PlcBit { get; set; } 
            public string AquisitionCycle { get; set; }
            public string Minimum { get; set; }
            public string Maximum { get; set; }
            public string Reference { get; set; }
            public string Notes { get; set; }
            public List<string> UsedLanguages { get; set; }
        }

        public class A
        {
            public int x { get; set; }
            public string b { get; set; }
            public C c { get; set; }
        }

        public class B
        {
            public string x { get; set; }
            public string b { get; set; }
            public int t { get; set; }
        public C c { get; set; }
    }

        public class C
        {
            public int x { get; set; }
        }
    }
}
