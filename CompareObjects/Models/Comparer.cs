﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CompareObjects.Models
{
    public class Comparer
    {
        public PropertyInfo Prop1;
        public PropertyInfo Prop2;
        public bool AreSame;
        public string  DifferentAttribute;
    }
}
